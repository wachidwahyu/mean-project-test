const mongoose = require('mongoose');

var inventory = mongoose.model('inventory',{
    InventoryName: {type: String},
    BuyDate: {type: Date},
    InventoryType: {type: String}
});

module.exports = {inventory};