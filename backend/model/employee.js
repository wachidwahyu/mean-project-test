const mongoose = require('mongoose');

var employee = mongoose.model('employee',{
    nip : {type: Number},
    name: {type: String},
    joindate: {type: Date},
    address: {type: String},
    salary: {type: Number},
    position : {type : String}
});

module.exports = {employee};