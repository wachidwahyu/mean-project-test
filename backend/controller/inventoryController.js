const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { inventory } = require('../model/inventory');



router.get('/', (req, res) => {
    inventory.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Inventory :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var inv = new inventory({
        InventoryName: req.body.InventoryName,
        BuyDate: req.body.BuyDate,
        InventoryType: req.body.InventoryType,
    });
    inv.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Inventory Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    inventory.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Inventory :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        var inv = ({
            InventoryName: req.body.InventoryName,
            BuyDate: req.body.BuyDate,
            InventoryType: req.body.InventoryType,
        });
    inventory.findByIdAndUpdate(req.params.id, { $set: inv }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Customer Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    inventory.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Inventory Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;