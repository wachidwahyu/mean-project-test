const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const {mongoose} = require('./db.js');
var inventoryController = require('./controller/inventoryController.js');
var employeeController = require('./controller/employeeController.js');

var app = express();
app.use(bodyParser.json());
app.use(cors({origin:'http://localhost:4200'}));

app.listen(8888, () =>console.log('Server started at port : 8888'));

app.use('/inventories',inventoryController);
app.use('/employees',employeeController);
