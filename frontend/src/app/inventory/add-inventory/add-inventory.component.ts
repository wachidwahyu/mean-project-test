import { Component, OnInit } from '@angular/core';
import { LabService } from '../../lab.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.css']
})
export class AddInventoryComponent implements OnInit {

  constructor(private service:LabService, private router: Router) { }

  inventory_arr : any;
  ngOnInit() {
  }

  onSubmit(){
    this.inventory_arr={
      "InventoryName" :(document.getElementById("InventoryName") as HTMLInputElement).value,
      "BuyDate" :(document.getElementById("BuyDate") as HTMLInputElement).value,
      "InventoryType" :(document.getElementById("InventoryType") as HTMLInputElement).value      
    }

    this.service.postInventory(this.inventory_arr).subscribe((res)=>{
      console.log(res);
      console.log(this.inventory_arr);
      this.router.navigate(["inventories"]);
    });
  }

  back(){
    this.router.navigate(["inventories"]);
  }

}
