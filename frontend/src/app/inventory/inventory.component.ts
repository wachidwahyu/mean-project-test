import { Component, OnInit } from '@angular/core';
import { LabService} from '../lab.service'
import { Router } from "@angular/router";

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  constructor(private service:LabService, private router:Router) { }

  inventory_from_db: any;
  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.service.getInventory().subscribe(data=>{
      console.log(JSON.stringify(data))
      return this.inventory_from_db= data
    });
  }


  addInventory(): void{
    console.log("ke add emp");
    this.router.navigate(["inventories/add"]);
  }

  deleteInventory(inventory): void{
    let r = confirm("Anda ingin menghapus data inventori "+inventory.InventoryName+"?");
    if (r == true) {
      this.service.deleteInventory(inventory).subscribe((res)=>{
        this.refresh();
      })
    }    
  }

  editInventory(inventory): void{
    console.log(inventory);
    window.localStorage.removeItem("editInventoryId");
    window.localStorage.setItem("editInventoryId", inventory._id+"");
    this.router.navigate(["inventories/edit"])
  }

}
