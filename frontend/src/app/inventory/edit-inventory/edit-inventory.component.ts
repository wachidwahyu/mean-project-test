import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LabService } from "../../lab.service";

@Component({
  selector: 'app-edit-inventory',
  templateUrl: './edit-inventory.component.html',
  styleUrls: ['./edit-inventory.component.css']
})
export class EditInventoryComponent implements OnInit {

  editForm: FormGroup;
  constructor(private router:Router, private serviceService: LabService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let inventoryId = window.localStorage.getItem("editInventoryId");
    if(!inventoryId){
      this.router.navigate(['inventories']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id:[],
      InventoryName:[],
      BuyDate:[],
      InventoryType:[],
      __v:[]
    });

    this.serviceService.getInventoryById(inventoryId).subscribe(data=>{
      this.editForm.setValue(data);
    });
  }

  onSubmit(){
    this.serviceService.putInventory(this.editForm.value).subscribe((data)=>{
      try{
        alert("Update SUccess!");
        this.router.navigate(["inventories"]);
      }
      catch(err){
        console.log(err.message);
      }
    });
  }

  back(){
    this.router.navigate(["inventories"]);
  }

}
