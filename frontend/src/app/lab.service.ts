import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Employee } from './model/employee.model';
import { Inventory } from './model/inventory.model';


@Injectable()
export class LabService {

  BASE_URL:string='http://localhost:8888/';
  urlEmploye:string='employees/';
  urlInventory:string='inventories/';
  constructor(private http:HttpClient) { }

  getEmployees(){
    return this.http.get(this.BASE_URL+this.urlEmploye);
  }

  getEmployeeById(empId: String):Observable<any>{
    return this.http.get<any>(this.BASE_URL+this.urlEmploye+empId);
  }
  postEmployee(emp: Employee){
    return this.http.post(this.BASE_URL+this.urlEmploye,emp);
  }

  deleteEmployee(emp: Employee){
    return this.http.delete(this.BASE_URL+this.urlEmploye+emp._id);
  }

  putEmployee(emp: Employee){
    return this.http.put(this.BASE_URL+this.urlEmploye+emp._id, emp);
  }

  //=================inventory=======================


  getInventory(){
    return this.http.get(this.BASE_URL+this.urlInventory);
  }

  getInventoryById(empId: String):Observable<any>{
    return this.http.get<any>(this.BASE_URL+this.urlInventory+empId);
  }
  postInventory(inv: Inventory){
    return this.http.post(this.BASE_URL+this.urlInventory,inv);
  }

  deleteInventory(inv: Inventory){
    return this.http.delete(this.BASE_URL+this.urlInventory+inv._id);
  }

  putInventory(inv: Inventory){
    return this.http.put(this.BASE_URL+this.urlInventory+inv._id, inv);
  }

}
