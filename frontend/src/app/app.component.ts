import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { LabService } from "./lab.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private service:LabService, private router: Router) { }
  title = 'our app';

  navigateEmployee():void{
    console.log("ke menu employee");
    this.router.navigate(["employees"]);
  }

  navigateInventory():void{
    console.log("ke menu Inventory");
    this.router.navigate(["inventories"]);
  }
}
