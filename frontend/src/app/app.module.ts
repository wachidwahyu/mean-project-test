import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { LabService } from './lab.service';
import { AddemployeeComponent } from './employee/addemployee/addemployee.component';
import { EditemployeeComponent } from './employee/editemployee/editemployee.component';
import { InventoryComponent } from './inventory/inventory.component';
import { AddInventoryComponent } from './inventory/add-inventory/add-inventory.component';
import { EditInventoryComponent } from './inventory/edit-inventory/edit-inventory.component';

const router : Routes =[
  {
    path:'employees', 
    component:EmployeeComponent
  },
  {
    path:'employees/add', 
    component:AddemployeeComponent
  },
  {
    path:'employees/edit', 
    component:EditemployeeComponent
  },
  {
    path:'inventories', 
    component:InventoryComponent
  },
  {
    path:'inventories/add', 
    component: AddInventoryComponent
  },
  {
    path:'inventories/edit', 
    component: EditInventoryComponent
  }
]


@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    AddemployeeComponent,
    EditemployeeComponent,
    InventoryComponent,
    AddInventoryComponent,
    EditInventoryComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(router),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[RouterModule],
  providers: [LabService],
  bootstrap: [AppComponent]
})
export class AppModule { }
