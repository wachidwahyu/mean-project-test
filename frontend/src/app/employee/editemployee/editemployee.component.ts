import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LabService } from "../../lab.service";

@Component({
  selector: 'app-editemployee',
  templateUrl: './editemployee.component.html',
  styleUrls: ['./editemployee.component.css']
})
export class EditemployeeComponent implements OnInit {
  editForm: FormGroup;
  constructor(private router:Router, private serviceService: LabService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let employeeId = window.localStorage.getItem("editEmployeeId");
    if(!employeeId){
      this.router.navigate(['employees']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id:[],
      nip:[],
      name:[],
      joindate:[],
      address:[],
      position:[],
      __v:[]
    });

    this.serviceService.getEmployeeById(employeeId).subscribe(data=>{
      this.editForm.setValue(data);
    });
  }

  onSubmit(){
    this.serviceService.putEmployee(this.editForm.value).subscribe((data)=>{
      try{
        alert("Update SUccess!");
        this.router.navigate(["employees"]);
      }
      catch(err){
        console.log(err.message);
      }
    });
  }

  back(){
    this.router.navigate(["employees"]);
  }

}
