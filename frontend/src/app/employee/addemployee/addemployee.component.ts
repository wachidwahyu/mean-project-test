import { Component, OnInit } from '@angular/core';
import { LabService } from '../../lab.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.css']
})
export class AddemployeeComponent implements OnInit {

  
  employee_arr : any;
  constructor(private service:LabService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.employee_arr={
      "nip" :(document.getElementById("nip") as HTMLInputElement).value,
      "name" :(document.getElementById("name") as HTMLInputElement).value,
      "joindate" :(document.getElementById("joindate") as HTMLInputElement).value,
      "address" :(document.getElementById("address") as HTMLInputElement).value,
      "position" :(document.getElementById("position") as HTMLInputElement).value
      
    }

    this.service.postEmployee(this.employee_arr).subscribe((res)=>{
      console.log(res);
      console.log(this.employee_arr);
      this.router.navigate(["employees"]);
    });
  }

  back(){
    this.router.navigate(["employees"]);
  }

}
