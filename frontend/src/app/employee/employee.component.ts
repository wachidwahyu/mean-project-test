import { Component, OnInit } from '@angular/core';
import { LabService } from '../lab.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private service:LabService, private router:Router) { }

  employees_from_db : any;
  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.service.getEmployees().subscribe(data=>{
      console.log(JSON.stringify(data))
      return this.employees_from_db= data
    });
  }

  addEmployee(): void{
    console.log("ke add emp");
    this.router.navigate(["employees/add"]);
  }

  deleteEmployee(employee): void{
    let r = confirm("Anda ingin menghapus data dengan nip "+employee.nip+"?");
    if (r == true) {
      this.service.deleteEmployee(employee).subscribe((res)=>{
        this.refresh();
      })
    }    
  }

  editEmployee(employee): void{
    console.log(employee);
    window.localStorage.removeItem("editEmployeeId");
    window.localStorage.setItem("editEmployeeId", employee._id+"");
    this.router.navigate(["employees/edit"])
  }
 
}
