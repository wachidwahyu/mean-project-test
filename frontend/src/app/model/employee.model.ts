export class Employee{
    _id: string;
    nip : string;
    name:string;
    joindate:Date;
    address:string;
    position:string;
}